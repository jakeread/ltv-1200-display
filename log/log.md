## Vent Display Log

`LTV-1200 --RS232-> Raspberry Pi Serialport --> Local Node.JS Bridge --WebSocket-> Browser Display`

Data streams 'realtime' packets from the LTV at ~ 10ms intervals (+ / - 5ms) (per the datasheet), but I observed some occasional drop-outs: I think these happen when the vent sends some other packet type (alarms). 

Otherwise, data is cached locally on the raspberry pi, and clients can request it in intervals: sending with their query the time-stamp of their most-recent slice: the pi should respond as best it can with a full update since then. 

Qualitative MVP video below, next steps probably:
 - pretty-ify,
 - run for ~ hours, verify no memory leaks etc 
 - do on RPI4, see if HZ can improve 
 - circuit for RS232 interface, RPI screen mount 
 - UI for system reset / reboot, 
 - UI for system state (vent connection OK? ws connection OK?) 
 - label flows 

Agree no need for oxygen, the ventilator will run from the internal turbine

Horizontal access should be time - 3-6 seconds.

Tidal volume is always positive so 0-1000 ml would cover all contingencies.

Flow (inspiratory < 100 L/min) and expiratory flow < 150 L/min.

Pressure also typically positive os scale is 0 - 80 should cover every possibility - most patients are ventilated at less than 45 cm H2O.

- get the lung 
- write the UI, make the circuit 

## Log

## 2021 01 01 

Drawing the circuit today. 

| Part | PN |
| --- | --- |
| Tranciever, RS232 | 296-9598-1-ND / MAX3238C |
| RJ12 Jack | WM5567CT-ND |
| 40 Pin RPI Female Header | S6104-ND | 

The way I have this set up, I mostly just have a small height issue. 

OK, this is pretty simple. 

![routed](log/2021-01-01_backpack-routed.png)
![schem](log/2021-01-01_backpack-schematic.png)

## 2020 11 18  

![running](log/2020-11-18_vent-display-mvp.mp4)

In order of appearance, this video should be:
- 'prox pressure'
- 'xdcr flow'
- volume 

## 2020 11 05 

### Yonder Phy

We have an RJ45 port, pinouts are 

| pin | sig | 
| --- | --- | 
| 1 | SGND | 
| 2 | TxD | 
| 3 | RxD | 
| 4 | CTS | 
| 5 | RTS | 
| 6 | SGND |

Seems like this should be straightforward - datasheet notes that data is uni-directional, my guess is that it just spews data out of the port, wouldn't that be nice. 

First move is to get it venting. [Setup Guide](https://bu.digication.com/bumc-ed1/ltv-ventilator-setup-and-initial-settings) 

OK, have woken it up with a glove, can confirm it has an RS232 out that just spews data - that's great. I do need an RS232 interface though, which swings -10 to +10v here. 

### RPI

pi  
vent 

- do rpi basic setup:
    - use mouse, keyboard, hdmi...
    - connect to internet, do updates 
- install node.js 
    - `$ curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -`
    - `$ sudo apt install -y nodejs`
- install raspi-serial
    - have to do some serialport config stuff 
    - might be some trix in here: the bluetooth module uses the serialport, so we have to wrestle for control 
- get vscode ? 

I think we want RPI4 for this, they have decent UART support now.
- https://learn.pi-supply.com/make/raspberry-pi-4-pinout/ 
- https://datasheets.raspberrypi.org/bcm2711/bcm2711-peripherals.pdf 

### Notes 

OK, turned the vent on today, and got characters from the RS232 port. Have an RPI setup and ready to test with some code tomorrow morning, and a temporary RS232-TTL adapter to splice in between. Some concern about the RPI's serial port being 'in use' when bluetooth module is being used, etc. RPI 4 has mucho ++ serial ports, will try to use one of those in the end run. 

So, first move tomorrow is getting the serial port hooked up & seeing if I can pull anything into JS, then is a little software architecting. 

## 2020 11 06

Serial on the RPI3 is a pain, have to disable a bunch of things:

https://openenergymonitor.org/forum-archive/node/12311.html 

OK, so I:
    - used node serialport (not the particular raspi-serial library)
    - set it up for /dev/ttyAMA0, the default port
    - baudrate to 60096 
    - use a bytelength parser 
    - disable the uart terminal / enable the uart hardware in `sudo raspi-config`
    - put `dtoverlay=pi3-disable-bt` in `/boot/config.txt`
    - did `sudo systemctl disable hciuart`
    - rebooted
    - now I am recieving bytes 

So I should decode some packets. 

Packets: decoded. Now I want the 'other end' of it, my bootstrap code / server / client. 

Cool, have a bootstrap up, just need to plug it into the ltv, get three plots up and one state-swapping thing, then it should be ahn MVP, next step would be a circuit / display. 

OK, I have the client / server architecture setup now... so just one more day at that and I should have an MVP, good enough to proceed with a circuit etc. 