// type set 

module.exports = {
    read: (type, buffer, start) => {
        switch(type){
            case 'uint8':
                return buffer[start]
            case 'uint16':
                return (buffer[start] & 255 | buffer[start + 1] << 8)
            case 'int16':
                return new Int16Array(buffer.slice(start, start + 2).buffer)[0]
            case 'uint32':
                return (buffer[start] | buffer[start + 1] << 8 | buffer[start + 2] << 16 | buffer[start + 3] << 3)
            default:
                console.log("bad type to read")
                break;
        }
    }
}