// ltv-client 

'use strict'

import { Plane, AutoPlot } from './panels.js'

let lastRecvdIndice = 0 

// now we can setup some canvases, 

let plane = new Plane()

let proxPresPlot = new AutoPlot(10, 10, 500, 200, "prox pressure")
let xdcrFlowPlot = new AutoPlot(10, 220, 500, 200, "xdcr flow")
let volumePlot = new AutoPlot(10, 430, 500, 200, "volume")

// to test these systems, the client (us) will kickstart a new process
// on the server, and try to establish connection to it.
console.log("making client-to-server request to start remote process,")
console.log("and connecting to it w/ new websocket")
// ok, let's ask to kick a process on the server,
// in response, we'll get it's IP and Port,
// then we can start a websocket client to connect there,
// automated remote-proc. w/ vPort & wss medium,
// for args, do '/processName.js?args=arg1,arg2'
let LOGPHY = false
jQuery.get('/startLocal/ltv-bridge.js', (res) => {
    if (res.includes('wss-addr:')) {
        let addr = res.substring(res.indexOf(':') + 2)
        if (addr.includes('ws://')) {
            console.log('starting socket to remote at', addr)
            let ws = new WebSocket(addr)
            ws.onopen = (evt) => {
                ws.onmessage = (msg) => {
                    msg = JSON.parse(msg.data)
                    if(msg.type == "plot update"){
                        if(msg.array.length > 0){
                            lastRecvdIndice = msg.array[msg.array.length - 1][0]
                            console.log('now last r: ', lastRecvdIndice)
                            // msg.array = [[indice, inspState, proxPres, xdcrFlow, volume],[indice...]]   
                            // can get loose with the indice for this hello-world, 
                            for(let i = 0; i < msg.array.length; i ++){
                                proxPresPlot.pushPt([msg.array[i][0], msg.array[i][2]])
                                xdcrFlowPlot.pushPt([msg.array[i][0], msg.array[i][3]])
                                volumePlot.pushPt([msg.array[i][0], msg.array[i][4]])
                            } 
                            proxPresPlot.redraw()
                            xdcrFlowPlot.redraw()
                            volumePlot.redraw()
                        } else {
                            console.error('zero len array return')
                        }
                    }
                }
                let reqData = () => {
                    if(ws.readyState == ws.OPEN){
                        ws.send(JSON.stringify({ 
                            type: "plot update request",
                            lastRecvdIndice: lastRecvdIndice 
                        }))
                        setTimeout(reqData, 100)    
                    } else {
                        console.error("WS Closed")
                    }
                }
                reqData() // start polling loop 
            }
            ws.onerror = (err) => {
                console.log('sckt err', err)
            }
            ws.onclose = (evt) => {
                console.log('sckt closed', evt)
            }
        }
    } else {
        console.error('machine link not established', res)
    }
})
