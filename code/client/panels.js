/*
gCodePanel.js

input gcodes 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

/*
notes on this thing

this is pretty straightforward at the moment, it'll read small gcodes
i.e. those used to mill circuits. for larger files, like 3DP files,
reading times / manipulating large streams of texts needs to be handled 
more intelligently, i.e. not rendering the whole file into the 'incoming' body. 

*/

'use strict'

import dt from './drawing/domtools.js'

// reference:
// spy from https://github.com/cncjs/gcode-parser/blob/master/src/index.js thx 
/*
G00:        move at rapids speed 
G01:        move at last G01 F<num>
G04 P<num>:  dwell for P milliseconds or X seconds 
G20:        set coordinates to inches 
G21:        set coordinates to mm 
G28:        do homing routine 
G90:        positions are absolute w/r/t zero 
G91:        positions are incremenetal w/r/t last moves 
G94:        feedrates are per minute 
*/
/*
F<num>:     set feedrate for modal G 
M03 S<num>: set clockwise rotation 
M04 S<num>: set counter-clockwise rotation 
M05:        stop spindle 
M83:        use extruder relative motion 
*/

function Plane() {
    // setup drawing env
    let plane = $('<div>').addClass('plane').get(0)
    let wrapper = $('#wrapper').get(0)
    // odd, but w/o this, scaling the plane & the background together introduces some numerical errs,
    // probably because the DOM is scaling a zero-size plane, or somesuch.
    $(plane).css('background', 'url("/client/drawing/bg.png")').css('width', '100px').css('height', '100px')
    let cs = 1 // current scale,
    let dft = { s: cs, x: 0, y: 0, ox: 0, oy: 0 } // default transform

    // init w/ defaults,
    dt.writeTransform(plane, dft)
    dt.writeBackgroundTransform(wrapper, dft)

    $(wrapper).append(plane)
}

function Button(xPlace, yPlace, width, height, text) {
    let btn = $('<div>').addClass('button')
        .text(text)
        .get(0)
    placeField(btn, width, height, xPlace, yPlace)
    return btn
}

function TextInput(xPlace, yPlace, width, height, text) {
    let input = $('<input>').addClass('inputwrap').get(0)
    input.value = text
    placeField(input, width, height, xPlace, yPlace)
    return input
}

let BTN_RED = 'rgb(242, 201, 201)'
let BTN_GRN = 'rgb(201, 242, 201)'
let BTN_YLW = 'rgb(240, 240, 180)'
let BTN_GREY = 'rgb(242, 242, 242)'
let BTN_HANGTIME = 1000
let BTN_ERRTIME = 2000

function AutoPlot(xPlace, yPlace, xSize, ySize, title) {
    let chart = $('<div>').get(0)
    $(chart).css('background-color', BTN_GREY)
    let uid = `lineChart_${Math.round(Math.random() * 1000)}_uid`
    $(chart).attr('id', uid)
    placeField(chart, xSize, ySize, xPlace, yPlace)

    // the data 
    var datas = [[0, 0]]
    var numToHold = 1000

    // our vars,
    var margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 50
    }

    if (!title) margin.top = 20

    var width = xSize - margin.left - margin.right
    var height = ySize - margin.top - margin.bottom
    var x = d3.scaleLinear().range([0, width])
    var y = d3.scaleLinear().range([height, 0])
    var thesvg = null

    // redraw 
    this.redraw = () => {
        var valueline = d3.line()
            .x(function (d) {
                return x(d[0])
            })
            .y(function (d) {
                return y(d[1])
            })
        // scale
        x.domain([d3.min(datas, function (d) {
            return d[0]
        }), d3.max(datas, function (d) {
            return d[0];
        })])
        y.domain([d3.min(datas, function (d) {
            return d[1]
        }), d3.max(datas, function (d) {
            return d[1];
        })])
        if (thesvg) {
            d3.select(`#${uid}`).selectAll("*").remove()
        }
        thesvg = d3.select(`#${uid}`).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        // write it?
        thesvg.append("path")
            .data([datas])
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", "4px")
            .attr("d", valueline)
        // write the x axis
        thesvg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x))
        // the y axis
        thesvg.append("g")
            .call(d3.axisLeft(y))
        // the title 
        if (title) {
            let info = ""
            if (datas.length > 0) {
                info = (datas[datas.length - 1][1]).toFixed(3)
            }
            thesvg.append("text")
                .attr("x", (width / 2))
                .attr("y", 0 - (margin.top / 2))
                .attr("text-anchor", "middle")
                .style("font-size", "12px")
                .style("font-family", "'Courier New', Courier, monospace")
                .text(`${title} ${info}`);
        }
    }
    // startup
    this.redraw()
    // add new pts 
    this.pushPt = (pt) => {
        datas.push(pt)
        if (datas.length > numToHold) {
            datas.shift()
        }
    }
    // set draw length 
    this.setDrawLength = (len) => {
        numToHold = len
        if (datas.length > numToHold) {
            datas = datas.slice(datas.length - numToHold)
        }
    }
}

let placeField = (field, width, height, xpos, ypos) => {
    $(field).css('position', 'absolute')
        .css('border', 'none')
        .css('width', `${width}px`)
        .css('height', `${height}px`)
    $($('.plane').get(0)).append(field)
    let dft = { s: 1, x: xpos, y: ypos, ox: 0, oy: 0 }
    dt.writeTransform(field, dft)
}

export { Plane, Button, TextInput, AutoPlot }